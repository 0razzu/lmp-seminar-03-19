import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


public class DataSet implements Iterable<Data> {
    private List<Data> datas;
    
    
    public DataSet() {
        datas = new LinkedList<>();
    }
    
    
    public void add(Data data) {
        datas.add(data);
    }
    
    
    public void sort() {
        datas.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
    }
    
    
    @Override
    public Iterator<Data> iterator() {
        return datas.iterator();
    }
}
